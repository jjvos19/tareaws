
package bo.com.fie.judis.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for nuevosEjecucionResp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="nuevosEjecucionResp">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codResp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descResp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nroOperacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="seccRegistros" type="{http://services.judis.fie.com.bo/}nuevosEjecucionReg" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nuevosEjecucionResp", propOrder = {
    "codResp",
    "descResp",
    "nroOperacion",
    "seccRegistros"
})
public class NuevosEjecucionResp {

    protected String codResp;
    protected String descResp;
    protected String nroOperacion;
    protected NuevosEjecucionReg seccRegistros;

    /**
     * Gets the value of the codResp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodResp() {
        return codResp;
    }

    /**
     * Sets the value of the codResp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodResp(String value) {
        this.codResp = value;
    }

    /**
     * Gets the value of the descResp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescResp() {
        return descResp;
    }

    /**
     * Sets the value of the descResp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescResp(String value) {
        this.descResp = value;
    }

    /**
     * Gets the value of the nroOperacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroOperacion() {
        return nroOperacion;
    }

    /**
     * Sets the value of the nroOperacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroOperacion(String value) {
        this.nroOperacion = value;
    }

    /**
     * Gets the value of the seccRegistros property.
     * 
     * @return
     *     possible object is
     *     {@link NuevosEjecucionReg }
     *     
     */
    public NuevosEjecucionReg getSeccRegistros() {
        return seccRegistros;
    }

    /**
     * Sets the value of the seccRegistros property.
     * 
     * @param value
     *     allowed object is
     *     {@link NuevosEjecucionReg }
     *     
     */
    public void setSeccRegistros(NuevosEjecucionReg value) {
        this.seccRegistros = value;
    }

}

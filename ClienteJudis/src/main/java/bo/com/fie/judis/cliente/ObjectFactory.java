
package bo.com.fie.judis.cliente;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the bo.com.fie.judis.cliente package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://services.judis.fie.com.bo/", "Exception");
    private final static QName _NuevosEjecucion_QNAME = new QName("http://services.judis.fie.com.bo/", "nuevosEjecucion");
    private final static QName _NuevosEjecucionResponse_QNAME = new QName("http://services.judis.fie.com.bo/", "nuevosEjecucionResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: bo.com.fie.judis.cliente
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link NuevosEjecucionResponse }
     * 
     */
    public NuevosEjecucionResponse createNuevosEjecucionResponse() {
        return new NuevosEjecucionResponse();
    }

    /**
     * Create an instance of {@link NuevosEjecucion }
     * 
     */
    public NuevosEjecucion createNuevosEjecucion() {
        return new NuevosEjecucion();
    }

    /**
     * Create an instance of {@link NuevosEjecucionSeccOperacion }
     * 
     */
    public NuevosEjecucionSeccOperacion createNuevosEjecucionSeccOperacion() {
        return new NuevosEjecucionSeccOperacion();
    }

    /**
     * Create an instance of {@link NuevosEjecucionParameters }
     * 
     */
    public NuevosEjecucionParameters createNuevosEjecucionParameters() {
        return new NuevosEjecucionParameters();
    }

    /**
     * Create an instance of {@link NuevosEjecucionReg }
     * 
     */
    public NuevosEjecucionReg createNuevosEjecucionReg() {
        return new NuevosEjecucionReg();
    }

    /**
     * Create an instance of {@link NuevosEjecucionReq }
     * 
     */
    public NuevosEjecucionReq createNuevosEjecucionReq() {
        return new NuevosEjecucionReq();
    }

    /**
     * Create an instance of {@link NuevoEjecucion }
     * 
     */
    public NuevoEjecucion createNuevoEjecucion() {
        return new NuevoEjecucion();
    }

    /**
     * Create an instance of {@link NuevosEjecucionResp }
     * 
     */
    public NuevosEjecucionResp createNuevosEjecucionResp() {
        return new NuevosEjecucionResp();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.judis.fie.com.bo/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NuevosEjecucion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.judis.fie.com.bo/", name = "nuevosEjecucion")
    public JAXBElement<NuevosEjecucion> createNuevosEjecucion(NuevosEjecucion value) {
        return new JAXBElement<NuevosEjecucion>(_NuevosEjecucion_QNAME, NuevosEjecucion.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NuevosEjecucionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.judis.fie.com.bo/", name = "nuevosEjecucionResponse")
    public JAXBElement<NuevosEjecucionResponse> createNuevosEjecucionResponse(NuevosEjecucionResponse value) {
        return new JAXBElement<NuevosEjecucionResponse>(_NuevosEjecucionResponse_QNAME, NuevosEjecucionResponse.class, null, value);
    }

}

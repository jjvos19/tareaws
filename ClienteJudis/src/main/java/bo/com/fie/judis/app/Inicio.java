/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bo.com.fie.judis.app;

import bo.com.fie.judis.cliente.ConsultaCreditos;
import bo.com.fie.judis.cliente.Exception_Exception;
import bo.com.fie.judis.cliente.IConsultaCreditos;
import bo.com.fie.judis.cliente.NuevoEjecucion;
import bo.com.fie.judis.cliente.NuevosEjecucionParameters;
import bo.com.fie.judis.cliente.NuevosEjecucionReq;
import bo.com.fie.judis.cliente.NuevosEjecucionResp;
import bo.com.fie.judis.cliente.NuevosEjecucionSeccOperacion;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <!-- INICIO COMENTARIO -->
 *
 *
 *
 * <!-- FIN COMENTARIO -->
 * <dl>
 * <dt>Proyecto:</dt><dd>ClienteJudis</dd>
 * <dt>Paquete:</dt><dd>bo.com.fie.judis.app</dd>
 * <dt>Archivo:</dt><dd>Inicio.java</dd>
 * <dt>Creado:</dt><dd>10-09-2019 - 06:32:25 PM</dd>
 * <dt>Cargo:</dt><dd>Desarrollador II</dd>
 * <dt>Area:</dt><dd>Gerencia Nacional de Tecnología y Procesos</dd>
 * </dl>
 *
 * @author Juan Jose Valencia Oropeza
 * @author Augutos Ochoa
 * @version 1.0 - 10-09-2019 06:32:25 PM
 */
public class Inicio {

    public static void main(String[] args) {
        try {
            ConsultaCreditos servicio = new ConsultaCreditos();
            IConsultaCreditos cliente = servicio.getConsultaCreditosPort();
            NuevosEjecucionReq consulta = new NuevosEjecucionReq();
            NuevosEjecucionSeccOperacion seccOperacion = new NuevosEjecucionSeccOperacion();
            seccOperacion.setOperacion("RPT-4001");
            NuevosEjecucionParameters parametros = new NuevosEjecucionParameters();
            parametros.setFeInicio("30/06/2019");
            seccOperacion.setSeccParameters(parametros);
            consulta.setSeccOperacion(seccOperacion);
            NuevosEjecucionResp resultado = cliente.nuevosEjecucion(consulta);
            for (NuevoEjecucion ne : resultado.getSeccRegistros().getRegistro()) {
                System.out.println(ne.toString());
            }
        } catch (Exception_Exception ex) {
            Logger.getLogger(Inicio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}


package bo.com.fie.judis.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for nuevosEjecucionReq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="nuevosEjecucionReq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="seccOperacion" type="{http://services.judis.fie.com.bo/}nuevosEjecucionSeccOperacion" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nuevosEjecucionReq", propOrder = {
    "seccOperacion"
})
public class NuevosEjecucionReq {

    protected NuevosEjecucionSeccOperacion seccOperacion;

    /**
     * Gets the value of the seccOperacion property.
     * 
     * @return
     *     possible object is
     *     {@link NuevosEjecucionSeccOperacion }
     *     
     */
    public NuevosEjecucionSeccOperacion getSeccOperacion() {
        return seccOperacion;
    }

    /**
     * Sets the value of the seccOperacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link NuevosEjecucionSeccOperacion }
     *     
     */
    public void setSeccOperacion(NuevosEjecucionSeccOperacion value) {
        this.seccOperacion = value;
    }

}

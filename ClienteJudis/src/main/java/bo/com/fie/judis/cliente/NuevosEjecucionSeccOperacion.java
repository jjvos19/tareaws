
package bo.com.fie.judis.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for nuevosEjecucionSeccOperacion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="nuevosEjecucionSeccOperacion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="entidadEmp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="feOperacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nroOperacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="seccParameters" type="{http://services.judis.fie.com.bo/}nuevosEjecucionParameters" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nuevosEjecucionSeccOperacion", propOrder = {
    "entidadEmp",
    "feOperacion",
    "nroOperacion",
    "operacion",
    "seccParameters"
})
public class NuevosEjecucionSeccOperacion {

    protected String entidadEmp;
    protected String feOperacion;
    protected String nroOperacion;
    protected String operacion;
    protected NuevosEjecucionParameters seccParameters;

    /**
     * Gets the value of the entidadEmp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntidadEmp() {
        return entidadEmp;
    }

    /**
     * Sets the value of the entidadEmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntidadEmp(String value) {
        this.entidadEmp = value;
    }

    /**
     * Gets the value of the feOperacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeOperacion() {
        return feOperacion;
    }

    /**
     * Sets the value of the feOperacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeOperacion(String value) {
        this.feOperacion = value;
    }

    /**
     * Gets the value of the nroOperacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNroOperacion() {
        return nroOperacion;
    }

    /**
     * Sets the value of the nroOperacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNroOperacion(String value) {
        this.nroOperacion = value;
    }

    /**
     * Gets the value of the operacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperacion() {
        return operacion;
    }

    /**
     * Sets the value of the operacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperacion(String value) {
        this.operacion = value;
    }

    /**
     * Gets the value of the seccParameters property.
     * 
     * @return
     *     possible object is
     *     {@link NuevosEjecucionParameters }
     *     
     */
    public NuevosEjecucionParameters getSeccParameters() {
        return seccParameters;
    }

    /**
     * Sets the value of the seccParameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link NuevosEjecucionParameters }
     *     
     */
    public void setSeccParameters(NuevosEjecucionParameters value) {
        this.seccParameters = value;
    }

}

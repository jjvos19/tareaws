package bo.com.fie.judis.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for nuevoEjecucion complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="nuevoEjecucion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="agencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="calificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ci" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="diasmora" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecvenci" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="garantia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="grupo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="monto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nrolinea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="operacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prevision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="responsable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tasa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipooperacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ultpago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nuevoEjecucion", propOrder = {
    "agencia",
    "calificacion",
    "ci",
    "diasmora",
    "estado",
    "fecha",
    "fecvenci",
    "garantia",
    "grupo",
    "moneda",
    "monto",
    "nombre",
    "nrolinea",
    "operacion",
    "prevision",
    "responsable",
    "saldo",
    "tasa",
    "telefono",
    "tipooperacion",
    "ultpago"
})
public class NuevoEjecucion {

    protected String agencia;
    protected String calificacion;
    protected String ci;
    protected String diasmora;
    protected String estado;
    protected String fecha;
    protected String fecvenci;
    protected String garantia;
    protected String grupo;
    protected String moneda;
    protected String monto;
    protected String nombre;
    protected String nrolinea;
    protected String operacion;
    protected String prevision;
    protected String responsable;
    protected String saldo;
    protected String tasa;
    protected String telefono;
    protected String tipooperacion;
    protected String ultpago;

    /**
     * Gets the value of the agencia property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getAgencia() {
        return agencia;
    }

    /**
     * Sets the value of the agencia property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setAgencia(String value) {
        this.agencia = value;
    }

    /**
     * Gets the value of the calificacion property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCalificacion() {
        return calificacion;
    }

    /**
     * Sets the value of the calificacion property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setCalificacion(String value) {
        this.calificacion = value;
    }

    /**
     * Gets the value of the ci property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCi() {
        return ci;
    }

    /**
     * Sets the value of the ci property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setCi(String value) {
        this.ci = value;
    }

    /**
     * Gets the value of the diasmora property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getDiasmora() {
        return diasmora;
    }

    /**
     * Sets the value of the diasmora property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setDiasmora(String value) {
        this.diasmora = value;
    }

    /**
     * Gets the value of the estado property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Sets the value of the estado property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setEstado(String value) {
        this.estado = value;
    }

    /**
     * Gets the value of the fecha property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * Sets the value of the fecha property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setFecha(String value) {
        this.fecha = value;
    }

    /**
     * Gets the value of the fecvenci property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getFecvenci() {
        return fecvenci;
    }

    /**
     * Sets the value of the fecvenci property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setFecvenci(String value) {
        this.fecvenci = value;
    }

    /**
     * Gets the value of the garantia property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getGarantia() {
        return garantia;
    }

    /**
     * Sets the value of the garantia property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setGarantia(String value) {
        this.garantia = value;
    }

    /**
     * Gets the value of the grupo property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getGrupo() {
        return grupo;
    }

    /**
     * Sets the value of the grupo property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setGrupo(String value) {
        this.grupo = value;
    }

    /**
     * Gets the value of the moneda property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getMoneda() {
        return moneda;
    }

    /**
     * Sets the value of the moneda property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setMoneda(String value) {
        this.moneda = value;
    }

    /**
     * Gets the value of the monto property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getMonto() {
        return monto;
    }

    /**
     * Sets the value of the monto property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setMonto(String value) {
        this.monto = value;
    }

    /**
     * Gets the value of the nombre property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Sets the value of the nombre property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setNombre(String value) {
        this.nombre = value;
    }

    /**
     * Gets the value of the nrolinea property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getNrolinea() {
        return nrolinea;
    }

    /**
     * Sets the value of the nrolinea property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setNrolinea(String value) {
        this.nrolinea = value;
    }

    /**
     * Gets the value of the operacion property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getOperacion() {
        return operacion;
    }

    /**
     * Sets the value of the operacion property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setOperacion(String value) {
        this.operacion = value;
    }

    /**
     * Gets the value of the prevision property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getPrevision() {
        return prevision;
    }

    /**
     * Sets the value of the prevision property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setPrevision(String value) {
        this.prevision = value;
    }

    /**
     * Gets the value of the responsable property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getResponsable() {
        return responsable;
    }

    /**
     * Sets the value of the responsable property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setResponsable(String value) {
        this.responsable = value;
    }

    /**
     * Gets the value of the saldo property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getSaldo() {
        return saldo;
    }

    /**
     * Sets the value of the saldo property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setSaldo(String value) {
        this.saldo = value;
    }

    /**
     * Gets the value of the tasa property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getTasa() {
        return tasa;
    }

    /**
     * Sets the value of the tasa property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setTasa(String value) {
        this.tasa = value;
    }

    /**
     * Gets the value of the telefono property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * Sets the value of the telefono property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setTelefono(String value) {
        this.telefono = value;
    }

    /**
     * Gets the value of the tipooperacion property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getTipooperacion() {
        return tipooperacion;
    }

    /**
     * Sets the value of the tipooperacion property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setTipooperacion(String value) {
        this.tipooperacion = value;
    }

    /**
     * Gets the value of the ultpago property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getUltpago() {
        return ultpago;
    }

    /**
     * Sets the value of the ultpago property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setUltpago(String value) {
        this.ultpago = value;
    }

    @Override
    public String toString() {
        return "NuevoEjecucion{" + "agencia=" + agencia + ", calificacion=" + calificacion + ", ci=" + ci + ", diasmora=" + diasmora + ", estado=" + estado + ", fecha=" + fecha + ", fecvenci=" + fecvenci + ", garantia=" + garantia + ", grupo=" + grupo + ", moneda=" + moneda + ", monto=" + monto + ", nombre=" + nombre + ", nrolinea=" + nrolinea + ", operacion=" + operacion + ", prevision=" + prevision + ", responsable=" + responsable + ", saldo=" + saldo + ", tasa=" + tasa + ", telefono=" + telefono + ", tipooperacion=" + tipooperacion + ", ultpago=" + ultpago + '}';
    }

}

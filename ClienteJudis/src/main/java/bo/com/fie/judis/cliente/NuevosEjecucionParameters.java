
package bo.com.fie.judis.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for nuevosEjecucionParameters complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="nuevosEjecucionParameters">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="feInicio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nuevosEjecucionParameters", propOrder = {
    "feInicio"
})
public class NuevosEjecucionParameters {

    protected String feInicio;

    /**
     * Gets the value of the feInicio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeInicio() {
        return feInicio;
    }

    /**
     * Sets the value of the feInicio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeInicio(String value) {
        this.feInicio = value;
    }

}
